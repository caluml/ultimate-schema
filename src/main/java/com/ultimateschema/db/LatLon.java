package com.ultimateschema.db;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LatLon {

	@Id
	private String id;

	private float lat;
	private float lon;

}
