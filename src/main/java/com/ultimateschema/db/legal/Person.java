package com.ultimateschema.db.legal;

import com.ultimateschema.db.physical.Human;

import javax.persistence.*;

@Entity
public class Person {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_person_human"))
	private Human human;
}
