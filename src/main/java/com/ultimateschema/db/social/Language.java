package com.ultimateschema.db.social;

import com.ultimateschema.db.physical.geographic.Country;

import javax.persistence.*;
import java.util.List;

@Entity
public class Language {

	@Id
	private String id;

	@ManyToMany
	@JoinColumn(foreignKey = @ForeignKey(name="fk_language_countriesspoken"))
	private List<Country> countriesSpoken;
}
