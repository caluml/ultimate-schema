package com.ultimateschema.db.social;

import javax.persistence.*;

@Entity
public class Dialect {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_dialect_language"))
	private Language language;
}
