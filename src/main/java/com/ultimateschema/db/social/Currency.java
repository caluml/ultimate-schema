package com.ultimateschema.db.social;

import com.ultimateschema.db.physical.geographic.Country;

import javax.persistence.*;
import java.util.List;

@Entity
public class Currency {

	@Id
	private String id;

	@ManyToMany
	@JoinTable(foreignKey = @ForeignKey(name="fk_currency_countries"))
	private List<Country> countriesUsed;
}
