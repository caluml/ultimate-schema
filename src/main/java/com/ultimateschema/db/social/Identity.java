package com.ultimateschema.db.social;

import com.ultimateschema.db.physical.Human;

import javax.persistence.*;

@Entity
public class Identity {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_identity_human"))
	private Human human;


	private String name;

	public Identity(Human human,
									String name) {
		this.human = human;
		this.name = name;
	}
}
