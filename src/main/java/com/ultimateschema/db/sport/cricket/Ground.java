package com.ultimateschema.db.sport.cricket;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Ground {

	@Id
	private String id;


	private final String name;

	private Ground(String name) {
		this.name = name;
	}

	public static Ground of(String name) {
		return new Ground(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
