package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;

@Entity
public class Match {

	@Id
	private String id;


	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_match_series"))
	private Series series;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_match_ground"))
	private Ground ground;

	public Match(Series series,
							 Ground ground) {
		this.series = series;
		this.ground = ground;
	}


	@Override
	public String toString() {
		return "the " + ground + " ground in " + series;
	}
}
