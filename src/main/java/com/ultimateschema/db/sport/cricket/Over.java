package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;

@Entity
public class Over {

	@Id
	private String id;


	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_over_session"))
	private Session session;

	private int over;

	public Over(Session session,
							int over) {
		this.session = session;
		this.over = over;
	}


	@Override
	public String toString() {
		return "the " + Ordinal.of(over) + " over in " + session;
	}
}
