package com.ultimateschema.db.sport.cricket;

import com.ultimateschema.db.physical.Human;

import javax.persistence.*;

@Entity
public class CricketPlayer {

	@Id
	private String id;


	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_cricketplayer_human"))
	private Human human;
}
