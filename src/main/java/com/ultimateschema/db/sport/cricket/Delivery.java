package com.ultimateschema.db.sport.cricket;

public enum Delivery {

	FAIR,
	WIDE,
	NO_BALL
}
