package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;

@Entity
public class Result {


	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_result_ball"))
	private Ball ball;

	private int runs;
	private int byes;
	private int extras;
	private Delivery delivery;
	private Dismissal dismissal;

	public Result(Ball ball,
								int runs,
								int byes,
								int extras,
								Delivery delivery,
								Dismissal dismissal) {
		this.ball = ball;
		this.runs = runs;
		this.byes = byes;
		this.extras = extras;
		this.delivery = delivery;
		this.dismissal = dismissal;
	}

	@Override
	public String toString() {
		return "a " + delivery + " delivery, scoring " + runs + " runs, " + byes + " byes resulting in " + dismissal + " of " + ball;
	}
}
