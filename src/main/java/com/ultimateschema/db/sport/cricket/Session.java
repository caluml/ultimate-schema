package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;

@Entity
public class Session {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_session_innings"))
	private final Innings innings;

	private final int day;

	private final String session;

	public Session(Innings innings,
								 int day,
								 String session) {
		this.innings = innings;
		this.day = day;
		this.session = session;
	}

	@Override
	public String toString() {
		return "the " + Ordinal.of(day) + " day " + session + " session in " + innings;
	}
}
