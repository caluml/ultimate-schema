package com.ultimateschema.db.sport.cricket;

import com.ultimateschema.db.physical.Human;

import javax.persistence.*;

@Entity
public class Ball {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_ball_over"))
	private Over over;

	private int number;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_ball_bowler"))
	private Human bowler;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_ball_batsman"))
	private Human batsman;

	public Ball(Over over,
							int number,
							Human bowler,
							Human batsman) {
		this.over = over;
		this.number = number;
		this.bowler = bowler;
		this.batsman = batsman;
	}


	@Override
	public String toString() {
		return "the " + Ordinal.of(number) + " ball from " + bowler + " to " + batsman + " in " + over;
	}
}
