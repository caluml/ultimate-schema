package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;

@Entity
public class Innings {

	@Id
	private String id;


	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_innings_match"))
	private Match match;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_innings_team"))
	private Team team;

	private int innings;

	public Innings(Match match,
								 Team team,
								 int innings) {
		this.match = match;
		this.team = team;
		this.innings = innings;
	}

	@Override
	public String toString() {
		return "the " + team + " " + Ordinal.of(innings) + " innings in " + match;
	}

}
