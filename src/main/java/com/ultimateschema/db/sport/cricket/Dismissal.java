package com.ultimateschema.db.sport.cricket;

public enum Dismissal {

	NONE,

	BOWLED,
	CAUGHT,
	LBW,
	RUN_OUT,
	STUMPED,

	RETIRED,
	HIT_BALL_TWICE,
	HIT_WICKET,
	OBSTRUCTING_THE_FIELD,
	TIMED_OUT,

	HANDLED_THE_BALL
}
