package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;
import java.util.List;

@Entity
public class Team {

	@Id
	private String id;

	
	private final String name;

	@OneToMany
	@JoinColumn(foreignKey = @ForeignKey(name="fk_team_players"))
	private List<CricketPlayer> players;

	private Team(String name) {
		this.name = name;
	}

	public static Team of(String name) {
		return new Team(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
