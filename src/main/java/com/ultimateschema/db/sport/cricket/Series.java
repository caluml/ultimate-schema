package com.ultimateschema.db.sport.cricket;

import javax.persistence.*;
import java.time.Year;

@Entity
public class Series {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_series_hometeam"))
	private Team homeTeam;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_series_awayteam"))
	private Team awayTeam;

	private Format format;

	private Year year;


	public Series(Team homeTeam,
								Team awayAway,
								Format format,
								Year year) {
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.format = format;
		this.year = year;
	}


	@Override
	public String toString() {
		return "the " + homeTeam + " v " + awayTeam + " " + year + " " + format + " series";
	}
}
