package com.ultimateschema.db.physical;

import javax.persistence.*;

@Entity
public class Human {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_human_father"))
	private Human father;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_human_mother"))
	private Human mother;
}
