package com.ultimateschema.db.physical;

import javax.persistence.*;

@Entity
public class Planet {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_planet_solarsystem"))
	private SolarSystem solarSystem;
}
