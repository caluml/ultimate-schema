package com.ultimateschema.db.physical;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Galaxy {

	@Id
	private String id;


	private String name;

	public Galaxy(String name) {
		this.name = name;
	}
}
