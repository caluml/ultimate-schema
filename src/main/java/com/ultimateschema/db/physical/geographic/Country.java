package com.ultimateschema.db.physical.geographic;

import javax.persistence.*;

@Entity
public class Country {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_country_landmass"))
	private Landmass landmass;

	private String name;
}
