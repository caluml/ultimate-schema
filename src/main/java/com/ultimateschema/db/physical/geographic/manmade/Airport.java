package com.ultimateschema.db.physical.geographic.manmade;

import com.ultimateschema.db.LatLon;

import javax.persistence.*;

@Entity
public class Airport {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_airport_latlon"))
	private LatLon latLon;
}
