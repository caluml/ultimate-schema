package com.ultimateschema.db.physical.geographic.manmade;

import javax.persistence.Entity;
import javax.persistence.Id;

//TODO: What denotes the end of one building, and the start of another? E.g. a terrace of houses, or a semi-detached house.
@Entity
public class Building {

	@Id
	private String id;

	//TODO: Should buildings have names?
	private String name;
}
