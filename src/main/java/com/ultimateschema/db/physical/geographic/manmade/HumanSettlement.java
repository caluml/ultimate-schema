package com.ultimateschema.db.physical.geographic.manmade;

import com.ultimateschema.db.physical.geographic.Country;

import javax.persistence.*;

@Entity
public class HumanSettlement {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_humansettlement_country"))
	private Country country;

	private String name;
}
