package com.ultimateschema.db.physical.geographic.manmade;

import javax.persistence.*;

// A dwelling is a whole or part of a building where humans reside
@Entity
public class Dwelling {

	@Id
	private String id;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_dwelling_building"))
	private Building building;
}
