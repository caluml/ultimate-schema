package com.ultimateschema.db.physical.geographic.manmade;

import javax.persistence.*;

@Entity
public class BuildingFloor {

	@Id
	private String id;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_floor_building"))
	private Building building;

	/*
		US and British use different floor numbering schemes. In the UK, the ground floor is 0. In the US it is 1.
	 */
	private String floor;
}
