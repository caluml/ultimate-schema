package com.ultimateschema.db.physical.geographic;

import com.ultimateschema.db.physical.Planet;

import javax.persistence.*;

@Entity
public class Landmass {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_landmass_planet"))
	private Planet planet;

	private String name;
}
