package com.ultimateschema.db.physical.geographic;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AddressLine {

	@Id
	private String id;


	private int lineOrder;

	private String line;
}
