package com.ultimateschema.db.physical.transport;

import com.ultimateschema.db.legal.Company;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CarModel {

	@Id
	private String id;

	@OneToOne
	private Company manufacturer;

	private String name;

	@OneToOne
	private VehicleEngine engine;

	// Possible fields?
	// Doors: 2, 3, 5
	// Wheels: 3, 4, 6
}
