package com.ultimateschema.db.physical.transport;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Car {

	@Id
	private String id;

	private String vin;

	private int year;

	@OneToOne
	private CarModel model;
}
