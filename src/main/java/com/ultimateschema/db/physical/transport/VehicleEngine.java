package com.ultimateschema.db.physical.transport;

import com.ultimateschema.db.legal.Company;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class VehicleEngine {

	@Id
	private String id;

	@OneToOne
	private Company manufacturer;

	// Possible fields
	// Engine type: internal combustion, electric
	// Valves
	// Cams
	// Piston configuration: Straight, Flat, V
	// Engine size: 1.4l, 200kW
}
