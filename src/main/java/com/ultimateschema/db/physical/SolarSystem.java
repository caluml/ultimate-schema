package com.ultimateschema.db.physical;

import javax.persistence.*;

@Entity
public class SolarSystem {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name="fk_solarsystem_galaxy"))
	private Galaxy galaxy;
}
