package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Family {

	@Id
	private String id;


	@OneToOne
	private Order order;

	private String name;
}
