package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Species {

	@Id
	private String id;


	@OneToOne
	private Genus genus;

	private String name;
}
