package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Subtribe {

	@Id
	private String id;

	@OneToOne
	private Tribe tribe;

	private String name;
}
