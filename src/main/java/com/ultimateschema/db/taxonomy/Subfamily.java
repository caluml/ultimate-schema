package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Subfamily {


	@Id
	private String id;

	@OneToOne
	private Family family;

	private String name;
}
