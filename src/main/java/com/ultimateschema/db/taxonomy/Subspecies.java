package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Subspecies {

	@Id
	private String id;

	@OneToOne
	private Species species;

	private String name;
}
