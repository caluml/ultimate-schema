package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "class")
public class Clas {

	@Id
	private String id;


	@OneToOne
	private Phylum phylum;

	private String name;
}
