package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Phylum {

	@Id
	private String id;


	@OneToOne
	private Kingdom kingdom;

	private String name;
}
