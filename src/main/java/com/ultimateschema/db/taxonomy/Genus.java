package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Genus {

	@Id
	private String id;


	@OneToOne
	private Subtribe subtribe;

	private String name;
}
