package com.ultimateschema.db.taxonomy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Tribe {


	@Id
	private String id;

	@OneToOne
	private Subfamily subfamily;

	private String name;
}
