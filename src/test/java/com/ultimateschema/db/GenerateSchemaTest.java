package com.ultimateschema.db;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = {
	"spring.jpa.properties.javax.persistence.schema-generation.create-source=metadata",
	"spring.jpa.properties.javax.persistence.schema-generation.scripts.action=create",
	"spring.jpa.properties.javax.persistence.schema-generation.scripts.create-target=ultimate-schema.sql"
})
class GenerateSchemaTest {

	@Test
	void contextLoads() {
	}

}
