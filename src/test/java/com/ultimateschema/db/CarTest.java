package com.ultimateschema.db;

import org.junit.jupiter.api.Test;

public class CarTest extends DbTest {

	@Test
	public void Car_test() {
		String company = id();
		jdbcTemplate.update("INSERT INTO company (id, name) VALUES (?, ?)", company, "Honda");

		String model = id();
		jdbcTemplate.update("INSERT INTO car_model (id, manufacturer_id, name) VALUES (?, ?, ?)", model, company, "Honda");

		String car = id();
		jdbcTemplate.update("INSERT INTO car (id, year, model_id) VALUES (?, ?, ?)", car, 2010, model);
	}

}
