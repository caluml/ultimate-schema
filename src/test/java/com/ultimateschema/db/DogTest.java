package com.ultimateschema.db;

import org.junit.jupiter.api.Test;

public class DogTest extends DbTest {

	@Test
	public void Dog() {
		String kingdom = id();
		jdbcTemplate.update("INSERT INTO kingdom (id, name) VALUES (?, ?)", kingdom, "Animalia");
		String phylum = id();
		jdbcTemplate.update("INSERT INTO phylum (id, name, kingdom_id) VALUES (?, ?, ?)", phylum, "Chordata", kingdom);
		String classId = id();
		jdbcTemplate.update("INSERT INTO class (id, name, phylum_id) VALUES (?, ?, ?)", classId, "Mammalia", phylum);
		String order = id();
		jdbcTemplate.update("INSERT INTO orderr (id, name, clas_id) VALUES (?, ?, ?)", order, "Carnivora", classId);
		String family = id();
		jdbcTemplate.update("INSERT INTO family (id, name, order_id) VALUES (?, ?, ?)", family, "Canidae", order);
		jdbcTemplate.update("INSERT INTO subfamily (id, name, family_id) VALUES (?, ?, ?)", id(), "Caninae", family);
		jdbcTemplate.update("INSERT INTO tribe (id, name) VALUES (?, ?)", id(), "Canini");
		jdbcTemplate.update("INSERT INTO subtribe (id, name) VALUES (?, ?)", id(), "Canina");
		jdbcTemplate.update("INSERT INTO genus (id, name) VALUES (?, ?)", id(), "Canis");
		jdbcTemplate.update("INSERT INTO species (id, name) VALUES (?, ?)", id(), "C. lupus");
		jdbcTemplate.update("INSERT INTO subspecies (id, name) VALUES (?, ?)", id(), "C. l. familiaris");
	}
}
