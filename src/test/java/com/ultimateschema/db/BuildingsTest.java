package com.ultimateschema.db;

import org.junit.jupiter.api.Test;

public class BuildingsTest extends DbTest {

	@Test
	public void Buckingham_Palace() {
		String landmass = id();
		jdbcTemplate.update("INSERT INTO landmass (id, name) VALUES (?, ?)", landmass, "Great Britain");

		String country = id();
		jdbcTemplate.update("INSERT INTO country (id, landmass_id, name) VALUES (?, ?, ?)", country, landmass, "England");

		String humanSettlement = id();
		jdbcTemplate.update("INSERT INTO human_settlement (id, country_id, name) VALUES (?, ?, ?)", humanSettlement, country, "London");

		//TODO: How should the address be modelled?


		//TODO: Should a building be referenced by address, or lat/lon, or both, or none?
		String building = id();
		jdbcTemplate.update("INSERT INTO building (id, name) VALUES (?, ?)", building, "Buckingham_Palace");
	}

}
