package com.ultimateschema.db;

import org.junit.jupiter.api.Test;

public class PeopleTest extends DbTest {

	@Test
	public void Barry_Humphries() {
		// One human...
		String human = id();
		jdbcTemplate.update("INSERT INTO human (id) VALUES (?)", human);

		// Three identities...
		jdbcTemplate.update("INSERT INTO identity (id, human_id, name) VALUES (?,?,?)", id(), human, "Barry Humphries");
		jdbcTemplate.update("INSERT INTO identity (id, human_id, name) VALUES (?,?,?)", id(), human, "Sir Les Patterson");
		jdbcTemplate.update("INSERT INTO identity (id, human_id, name) VALUES (?,?,?)", id(), human, "Dame Edna Everage");
	}

}
