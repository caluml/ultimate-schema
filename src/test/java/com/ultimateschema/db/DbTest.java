package com.ultimateschema.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Base64;
import java.util.Random;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public abstract class DbTest {

	@Autowired
	JdbcTemplate jdbcTemplate;


	String id() {
		Random random = new Random();
		byte[] bytes = new byte[128];
		random.nextBytes(bytes);
		return Base64.getEncoder().encodeToString(bytes).substring(0, 8);
	}
}
