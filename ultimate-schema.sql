
    create table address_line (
       id varchar(255) not null,
        line varchar(255),
        line_order int4 not null,
        primary key (id)
    )

    create table airport (
       id varchar(255) not null,
        lat_lon_id varchar(255),
        primary key (id)
    )

    create table ball (
       id varchar(255) not null,
        number int4 not null,
        batsman_id varchar(255),
        bowler_id varchar(255),
        over_id varchar(255),
        primary key (id)
    )

    create table building (
       id varchar(255) not null,
        primary key (id)
    )

    create table building_floor (
       id varchar(255) not null,
        floor varchar(255),
        building_id varchar(255),
        primary key (id)
    )

    create table car (
       id varchar(255) not null,
        vin varchar(255),
        year int4 not null,
        model_id varchar(255),
        primary key (id)
    )

    create table car_model (
       id varchar(255) not null,
        name varchar(255),
        manufacturer_id varchar(255),
        primary key (id)
    )

    create table class (
       id varchar(255) not null,
        name varchar(255),
        phylum_id varchar(255),
        primary key (id)
    )

    create table company (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table country (
       id varchar(255) not null,
        name varchar(255),
        landmass_id varchar(255),
        primary key (id)
    )

    create table cricket_player (
       id varchar(255) not null,
        human_id varchar(255),
        players_id varchar(255),
        primary key (id)
    )

    create table currency (
       id varchar(255) not null,
        primary key (id)
    )

    create table currency_countries_used (
       currency_id varchar(255) not null,
        countries_used_id varchar(255) not null
    )

    create table dialect (
       id varchar(255) not null,
        language_id varchar(255),
        primary key (id)
    )

    create table dwelling (
       id varchar(255) not null,
        building_id varchar(255),
        primary key (id)
    )

    create table family (
       id varchar(255) not null,
        name varchar(255),
        order_id varchar(255),
        primary key (id)
    )

    create table galaxy (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table genus (
       id varchar(255) not null,
        name varchar(255),
        subtribe_id varchar(255),
        primary key (id)
    )

    create table ground (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table human (
       id varchar(255) not null,
        father_id varchar(255),
        mother_id varchar(255),
        primary key (id)
    )

    create table human_settlement (
       id varchar(255) not null,
        name varchar(255),
        country_id varchar(255),
        primary key (id)
    )

    create table identity (
       id varchar(255) not null,
        name varchar(255),
        human_id varchar(255),
        primary key (id)
    )

    create table innings (
       id varchar(255) not null,
        innings int4 not null,
        match_id varchar(255),
        team_id varchar(255),
        primary key (id)
    )

    create table kingdom (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table landmass (
       id varchar(255) not null,
        name varchar(255),
        planet_id varchar(255),
        primary key (id)
    )

    create table language (
       id varchar(255) not null,
        primary key (id)
    )

    create table language_countries_spoken (
       language_id varchar(255) not null,
        countries_spoken_id varchar(255) not null
    )

    create table lat_lon (
       id varchar(255) not null,
        lat float4 not null,
        lon float4 not null,
        primary key (id)
    )

    create table match (
       id varchar(255) not null,
        ground_id varchar(255),
        series_id varchar(255),
        primary key (id)
    )

    create table orderr (
       id varchar(255) not null,
        name varchar(255),
        clas_id varchar(255),
        primary key (id)
    )

    create table over (
       id varchar(255) not null,
        over int4 not null,
        session_id varchar(255),
        primary key (id)
    )

    create table person (
       id varchar(255) not null,
        human_id varchar(255),
        primary key (id)
    )

    create table phylum (
       id varchar(255) not null,
        name varchar(255),
        kingdom_id varchar(255),
        primary key (id)
    )

    create table planet (
       id varchar(255) not null,
        solar_system_id varchar(255),
        primary key (id)
    )

    create table result (
       id varchar(255) not null,
        byes int4 not null,
        delivery int4,
        dismissal int4,
        extras int4 not null,
        runs int4 not null,
        ball_id varchar(255),
        primary key (id)
    )

    create table series (
       id varchar(255) not null,
        format int4,
        year bytea,
        away_team_id varchar(255),
        home_team_id varchar(255),
        primary key (id)
    )

    create table session (
       id varchar(255) not null,
        day int4 not null,
        session varchar(255),
        innings_id varchar(255),
        primary key (id)
    )

    create table solar_system (
       id varchar(255) not null,
        galaxy_id varchar(255),
        primary key (id)
    )

    create table species (
       id varchar(255) not null,
        name varchar(255),
        genus_id varchar(255),
        primary key (id)
    )

    create table subfamily (
       id varchar(255) not null,
        name varchar(255),
        family_id varchar(255),
        primary key (id)
    )

    create table subspecies (
       id varchar(255) not null,
        name varchar(255),
        species_id varchar(255),
        primary key (id)
    )

    create table subtribe (
       id varchar(255) not null,
        name varchar(255),
        tribe_id varchar(255),
        primary key (id)
    )

    create table team (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table tribe (
       id varchar(255) not null,
        name varchar(255),
        subfamily_id varchar(255),
        primary key (id)
    )

    create table vehicle_manufacturer (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    alter table if exists airport 
       add constraint fk_airport_latlon 
       foreign key (lat_lon_id) 
       references lat_lon

    alter table if exists ball 
       add constraint fk_ball_batsman 
       foreign key (batsman_id) 
       references human

    alter table if exists ball 
       add constraint fk_ball_bowler 
       foreign key (bowler_id) 
       references human

    alter table if exists ball 
       add constraint fk_ball_over 
       foreign key (over_id) 
       references over

    alter table if exists building_floor 
       add constraint fk_floor_building 
       foreign key (building_id) 
       references building

    alter table if exists car 
       add constraint FKdr4movr77cm8s34r8jtv9ajvi 
       foreign key (model_id) 
       references car_model

    alter table if exists car_model 
       add constraint FKbhf1nita7970hg0wyeo4l4v3v 
       foreign key (manufacturer_id) 
       references company

    alter table if exists class 
       add constraint FKhd648j0bo0ujuy4ubivfx6555 
       foreign key (phylum_id) 
       references phylum

    alter table if exists country 
       add constraint fk_country_landmass 
       foreign key (landmass_id) 
       references landmass

    alter table if exists cricket_player 
       add constraint fk_cricketplayer_human 
       foreign key (human_id) 
       references human

    alter table if exists cricket_player 
       add constraint fk_team_players 
       foreign key (players_id) 
       references team

    alter table if exists currency_countries_used 
       add constraint FKr4qcylafjm27gbofcfv3lo7md 
       foreign key (countries_used_id) 
       references country

    alter table if exists currency_countries_used 
       add constraint fk_currency_countries 
       foreign key (currency_id) 
       references currency

    alter table if exists dialect 
       add constraint fk_dialect_language 
       foreign key (language_id) 
       references language

    alter table if exists dwelling 
       add constraint fk_dwelling_building 
       foreign key (building_id) 
       references building

    alter table if exists family 
       add constraint FKjvyd83xv96h3af55pnygypluu 
       foreign key (order_id) 
       references orderr

    alter table if exists genus 
       add constraint FKeyuifdoiqt6wo13n8hmqtgoq2 
       foreign key (subtribe_id) 
       references subtribe

    alter table if exists human 
       add constraint fk_human_father 
       foreign key (father_id) 
       references human

    alter table if exists human 
       add constraint fk_human_mother 
       foreign key (mother_id) 
       references human

    alter table if exists human_settlement 
       add constraint fk_humansettlement_country 
       foreign key (country_id) 
       references country

    alter table if exists identity 
       add constraint fk_identity_human 
       foreign key (human_id) 
       references human

    alter table if exists innings 
       add constraint fk_innings_match 
       foreign key (match_id) 
       references match

    alter table if exists innings 
       add constraint fk_innings_team 
       foreign key (team_id) 
       references team

    alter table if exists landmass 
       add constraint fk_landmass_planet 
       foreign key (planet_id) 
       references planet

    alter table if exists language_countries_spoken 
       add constraint FK2lqp9hudewybdoq1akpj6sft1 
       foreign key (countries_spoken_id) 
       references country

    alter table if exists language_countries_spoken 
       add constraint fk_language_countriesspoken 
       foreign key (language_id) 
       references language

    alter table if exists match 
       add constraint fk_match_ground 
       foreign key (ground_id) 
       references ground

    alter table if exists match 
       add constraint fk_match_series 
       foreign key (series_id) 
       references series

    alter table if exists orderr 
       add constraint FKghgfubyq9els64h28y6ogghti 
       foreign key (clas_id) 
       references class

    alter table if exists over 
       add constraint fk_over_session 
       foreign key (session_id) 
       references session

    alter table if exists person 
       add constraint fk_person_human 
       foreign key (human_id) 
       references human

    alter table if exists phylum 
       add constraint FKjasntrmqv0lkja5xcpul1h73o 
       foreign key (kingdom_id) 
       references kingdom

    alter table if exists planet 
       add constraint fk_planet_solarsystem 
       foreign key (solar_system_id) 
       references solar_system

    alter table if exists result 
       add constraint fk_result_ball 
       foreign key (ball_id) 
       references ball

    alter table if exists series 
       add constraint fk_series_awayteam 
       foreign key (away_team_id) 
       references team

    alter table if exists series 
       add constraint fk_series_hometeam 
       foreign key (home_team_id) 
       references team

    alter table if exists session 
       add constraint fk_session_innings 
       foreign key (innings_id) 
       references innings

    alter table if exists solar_system 
       add constraint fk_solarsystem_galaxy 
       foreign key (galaxy_id) 
       references galaxy

    alter table if exists species 
       add constraint FKcsgr3364mjx8kmkklv8sg5km 
       foreign key (genus_id) 
       references genus

    alter table if exists subfamily 
       add constraint FKbdnfo5coqk5eqlqic998k2wvq 
       foreign key (family_id) 
       references family

    alter table if exists subspecies 
       add constraint FKdue4cnw1xnbmt2aacvcw6i8vd 
       foreign key (species_id) 
       references species

    alter table if exists subtribe 
       add constraint FKm5fkrovx8y0u1fdp1slftwb0o 
       foreign key (tribe_id) 
       references tribe

    alter table if exists tribe 
       add constraint FKri14ulu7k2u6tx08jowxqpe4w 
       foreign key (subfamily_id) 
       references subfamily

    create table address_line (
       id varchar(255) not null,
        line varchar(255),
        line_order int4 not null,
        primary key (id)
    )

    create table airport (
       id varchar(255) not null,
        lat_lon_id varchar(255),
        primary key (id)
    )

    create table ball (
       id varchar(255) not null,
        number int4 not null,
        batsman_id varchar(255),
        bowler_id varchar(255),
        over_id varchar(255),
        primary key (id)
    )

    create table building (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table building_floor (
       id varchar(255) not null,
        floor varchar(255),
        building_id varchar(255),
        primary key (id)
    )

    create table car (
       id varchar(255) not null,
        vin varchar(255),
        year int4 not null,
        model_id varchar(255),
        primary key (id)
    )

    create table car_model (
       id varchar(255) not null,
        name varchar(255),
        manufacturer_id varchar(255),
        primary key (id)
    )

    create table class (
       id varchar(255) not null,
        name varchar(255),
        phylum_id varchar(255),
        primary key (id)
    )

    create table company (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table country (
       id varchar(255) not null,
        name varchar(255),
        landmass_id varchar(255),
        primary key (id)
    )

    create table cricket_player (
       id varchar(255) not null,
        human_id varchar(255),
        players_id varchar(255),
        primary key (id)
    )

    create table currency (
       id varchar(255) not null,
        primary key (id)
    )

    create table currency_countries_used (
       currency_id varchar(255) not null,
        countries_used_id varchar(255) not null
    )

    create table dialect (
       id varchar(255) not null,
        language_id varchar(255),
        primary key (id)
    )

    create table dwelling (
       id varchar(255) not null,
        building_id varchar(255),
        primary key (id)
    )

    create table family (
       id varchar(255) not null,
        name varchar(255),
        order_id varchar(255),
        primary key (id)
    )

    create table galaxy (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table genus (
       id varchar(255) not null,
        name varchar(255),
        subtribe_id varchar(255),
        primary key (id)
    )

    create table ground (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table human (
       id varchar(255) not null,
        father_id varchar(255),
        mother_id varchar(255),
        primary key (id)
    )

    create table human_settlement (
       id varchar(255) not null,
        name varchar(255),
        country_id varchar(255),
        primary key (id)
    )

    create table identity (
       id varchar(255) not null,
        name varchar(255),
        human_id varchar(255),
        primary key (id)
    )

    create table innings (
       id varchar(255) not null,
        innings int4 not null,
        match_id varchar(255),
        team_id varchar(255),
        primary key (id)
    )

    create table kingdom (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table landmass (
       id varchar(255) not null,
        name varchar(255),
        planet_id varchar(255),
        primary key (id)
    )

    create table language (
       id varchar(255) not null,
        primary key (id)
    )

    create table language_countries_spoken (
       language_id varchar(255) not null,
        countries_spoken_id varchar(255) not null
    )

    create table lat_lon (
       id varchar(255) not null,
        lat float4 not null,
        lon float4 not null,
        primary key (id)
    )

    create table match (
       id varchar(255) not null,
        ground_id varchar(255),
        series_id varchar(255),
        primary key (id)
    )

    create table orderr (
       id varchar(255) not null,
        name varchar(255),
        clas_id varchar(255),
        primary key (id)
    )

    create table over (
       id varchar(255) not null,
        over int4 not null,
        session_id varchar(255),
        primary key (id)
    )

    create table person (
       id varchar(255) not null,
        human_id varchar(255),
        primary key (id)
    )

    create table phylum (
       id varchar(255) not null,
        name varchar(255),
        kingdom_id varchar(255),
        primary key (id)
    )

    create table planet (
       id varchar(255) not null,
        solar_system_id varchar(255),
        primary key (id)
    )

    create table result (
       id varchar(255) not null,
        byes int4 not null,
        delivery int4,
        dismissal int4,
        extras int4 not null,
        runs int4 not null,
        ball_id varchar(255),
        primary key (id)
    )

    create table series (
       id varchar(255) not null,
        format int4,
        year bytea,
        away_team_id varchar(255),
        home_team_id varchar(255),
        primary key (id)
    )

    create table session (
       id varchar(255) not null,
        day int4 not null,
        session varchar(255),
        innings_id varchar(255),
        primary key (id)
    )

    create table solar_system (
       id varchar(255) not null,
        galaxy_id varchar(255),
        primary key (id)
    )

    create table species (
       id varchar(255) not null,
        name varchar(255),
        genus_id varchar(255),
        primary key (id)
    )

    create table subfamily (
       id varchar(255) not null,
        name varchar(255),
        family_id varchar(255),
        primary key (id)
    )

    create table subspecies (
       id varchar(255) not null,
        name varchar(255),
        species_id varchar(255),
        primary key (id)
    )

    create table subtribe (
       id varchar(255) not null,
        name varchar(255),
        tribe_id varchar(255),
        primary key (id)
    )

    create table team (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table tribe (
       id varchar(255) not null,
        name varchar(255),
        subfamily_id varchar(255),
        primary key (id)
    )

    create table vehicle_manufacturer (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    alter table if exists airport 
       add constraint fk_airport_latlon 
       foreign key (lat_lon_id) 
       references lat_lon

    alter table if exists ball 
       add constraint fk_ball_batsman 
       foreign key (batsman_id) 
       references human

    alter table if exists ball 
       add constraint fk_ball_bowler 
       foreign key (bowler_id) 
       references human

    alter table if exists ball 
       add constraint fk_ball_over 
       foreign key (over_id) 
       references over

    alter table if exists building_floor 
       add constraint fk_floor_building 
       foreign key (building_id) 
       references building

    alter table if exists car 
       add constraint FKdr4movr77cm8s34r8jtv9ajvi 
       foreign key (model_id) 
       references car_model

    alter table if exists car_model 
       add constraint FKbhf1nita7970hg0wyeo4l4v3v 
       foreign key (manufacturer_id) 
       references company

    alter table if exists class 
       add constraint FKhd648j0bo0ujuy4ubivfx6555 
       foreign key (phylum_id) 
       references phylum

    alter table if exists country 
       add constraint fk_country_landmass 
       foreign key (landmass_id) 
       references landmass

    alter table if exists cricket_player 
       add constraint fk_cricketplayer_human 
       foreign key (human_id) 
       references human

    alter table if exists cricket_player 
       add constraint fk_team_players 
       foreign key (players_id) 
       references team

    alter table if exists currency_countries_used 
       add constraint FKr4qcylafjm27gbofcfv3lo7md 
       foreign key (countries_used_id) 
       references country

    alter table if exists currency_countries_used 
       add constraint fk_currency_countries 
       foreign key (currency_id) 
       references currency

    alter table if exists dialect 
       add constraint fk_dialect_language 
       foreign key (language_id) 
       references language

    alter table if exists dwelling 
       add constraint fk_dwelling_building 
       foreign key (building_id) 
       references building

    alter table if exists family 
       add constraint FKjvyd83xv96h3af55pnygypluu 
       foreign key (order_id) 
       references orderr

    alter table if exists genus 
       add constraint FKeyuifdoiqt6wo13n8hmqtgoq2 
       foreign key (subtribe_id) 
       references subtribe

    alter table if exists human 
       add constraint fk_human_father 
       foreign key (father_id) 
       references human

    alter table if exists human 
       add constraint fk_human_mother 
       foreign key (mother_id) 
       references human

    alter table if exists human_settlement 
       add constraint fk_humansettlement_country 
       foreign key (country_id) 
       references country

    alter table if exists identity 
       add constraint fk_identity_human 
       foreign key (human_id) 
       references human

    alter table if exists innings 
       add constraint fk_innings_match 
       foreign key (match_id) 
       references match

    alter table if exists innings 
       add constraint fk_innings_team 
       foreign key (team_id) 
       references team

    alter table if exists landmass 
       add constraint fk_landmass_planet 
       foreign key (planet_id) 
       references planet

    alter table if exists language_countries_spoken 
       add constraint FK2lqp9hudewybdoq1akpj6sft1 
       foreign key (countries_spoken_id) 
       references country

    alter table if exists language_countries_spoken 
       add constraint fk_language_countriesspoken 
       foreign key (language_id) 
       references language

    alter table if exists match 
       add constraint fk_match_ground 
       foreign key (ground_id) 
       references ground

    alter table if exists match 
       add constraint fk_match_series 
       foreign key (series_id) 
       references series

    alter table if exists orderr 
       add constraint FKghgfubyq9els64h28y6ogghti 
       foreign key (clas_id) 
       references class

    alter table if exists over 
       add constraint fk_over_session 
       foreign key (session_id) 
       references session

    alter table if exists person 
       add constraint fk_person_human 
       foreign key (human_id) 
       references human

    alter table if exists phylum 
       add constraint FKjasntrmqv0lkja5xcpul1h73o 
       foreign key (kingdom_id) 
       references kingdom

    alter table if exists planet 
       add constraint fk_planet_solarsystem 
       foreign key (solar_system_id) 
       references solar_system

    alter table if exists result 
       add constraint fk_result_ball 
       foreign key (ball_id) 
       references ball

    alter table if exists series 
       add constraint fk_series_awayteam 
       foreign key (away_team_id) 
       references team

    alter table if exists series 
       add constraint fk_series_hometeam 
       foreign key (home_team_id) 
       references team

    alter table if exists session 
       add constraint fk_session_innings 
       foreign key (innings_id) 
       references innings

    alter table if exists solar_system 
       add constraint fk_solarsystem_galaxy 
       foreign key (galaxy_id) 
       references galaxy

    alter table if exists species 
       add constraint FKcsgr3364mjx8kmkklv8sg5km 
       foreign key (genus_id) 
       references genus

    alter table if exists subfamily 
       add constraint FKbdnfo5coqk5eqlqic998k2wvq 
       foreign key (family_id) 
       references family

    alter table if exists subspecies 
       add constraint FKdue4cnw1xnbmt2aacvcw6i8vd 
       foreign key (species_id) 
       references species

    alter table if exists subtribe 
       add constraint FKm5fkrovx8y0u1fdp1slftwb0o 
       foreign key (tribe_id) 
       references tribe

    alter table if exists tribe 
       add constraint FKri14ulu7k2u6tx08jowxqpe4w 
       foreign key (subfamily_id) 
       references subfamily

    create table address_line (
       id varchar(255) not null,
        line varchar(255),
        line_order int4 not null,
        primary key (id)
    )

    create table airport (
       id varchar(255) not null,
        lat_lon_id varchar(255),
        primary key (id)
    )

    create table ball (
       id varchar(255) not null,
        number int4 not null,
        batsman_id varchar(255),
        bowler_id varchar(255),
        over_id varchar(255),
        primary key (id)
    )

    create table building (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table building_floor (
       id varchar(255) not null,
        floor varchar(255),
        building_id varchar(255),
        primary key (id)
    )

    create table car (
       id varchar(255) not null,
        vin varchar(255),
        year int4 not null,
        model_id varchar(255),
        primary key (id)
    )

    create table car_model (
       id varchar(255) not null,
        name varchar(255),
        engine_id varchar(255),
        manufacturer_id varchar(255),
        primary key (id)
    )

    create table class (
       id varchar(255) not null,
        name varchar(255),
        phylum_id varchar(255),
        primary key (id)
    )

    create table company (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table country (
       id varchar(255) not null,
        name varchar(255),
        landmass_id varchar(255),
        primary key (id)
    )

    create table cricket_player (
       id varchar(255) not null,
        human_id varchar(255),
        players_id varchar(255),
        primary key (id)
    )

    create table currency (
       id varchar(255) not null,
        primary key (id)
    )

    create table currency_countries_used (
       currency_id varchar(255) not null,
        countries_used_id varchar(255) not null
    )

    create table dialect (
       id varchar(255) not null,
        language_id varchar(255),
        primary key (id)
    )

    create table dwelling (
       id varchar(255) not null,
        building_id varchar(255),
        primary key (id)
    )

    create table family (
       id varchar(255) not null,
        name varchar(255),
        order_id varchar(255),
        primary key (id)
    )

    create table galaxy (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table genus (
       id varchar(255) not null,
        name varchar(255),
        subtribe_id varchar(255),
        primary key (id)
    )

    create table ground (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table human (
       id varchar(255) not null,
        father_id varchar(255),
        mother_id varchar(255),
        primary key (id)
    )

    create table human_settlement (
       id varchar(255) not null,
        name varchar(255),
        country_id varchar(255),
        primary key (id)
    )

    create table identity (
       id varchar(255) not null,
        name varchar(255),
        human_id varchar(255),
        primary key (id)
    )

    create table innings (
       id varchar(255) not null,
        innings int4 not null,
        match_id varchar(255),
        team_id varchar(255),
        primary key (id)
    )

    create table kingdom (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table landmass (
       id varchar(255) not null,
        name varchar(255),
        planet_id varchar(255),
        primary key (id)
    )

    create table language (
       id varchar(255) not null,
        primary key (id)
    )

    create table language_countries_spoken (
       language_id varchar(255) not null,
        countries_spoken_id varchar(255) not null
    )

    create table lat_lon (
       id varchar(255) not null,
        lat float4 not null,
        lon float4 not null,
        primary key (id)
    )

    create table match (
       id varchar(255) not null,
        ground_id varchar(255),
        series_id varchar(255),
        primary key (id)
    )

    create table orderr (
       id varchar(255) not null,
        name varchar(255),
        clas_id varchar(255),
        primary key (id)
    )

    create table over (
       id varchar(255) not null,
        over int4 not null,
        session_id varchar(255),
        primary key (id)
    )

    create table person (
       id varchar(255) not null,
        human_id varchar(255),
        primary key (id)
    )

    create table phylum (
       id varchar(255) not null,
        name varchar(255),
        kingdom_id varchar(255),
        primary key (id)
    )

    create table planet (
       id varchar(255) not null,
        solar_system_id varchar(255),
        primary key (id)
    )

    create table result (
       id varchar(255) not null,
        byes int4 not null,
        delivery int4,
        dismissal int4,
        extras int4 not null,
        runs int4 not null,
        ball_id varchar(255),
        primary key (id)
    )

    create table series (
       id varchar(255) not null,
        format int4,
        year bytea,
        away_team_id varchar(255),
        home_team_id varchar(255),
        primary key (id)
    )

    create table session (
       id varchar(255) not null,
        day int4 not null,
        session varchar(255),
        innings_id varchar(255),
        primary key (id)
    )

    create table solar_system (
       id varchar(255) not null,
        galaxy_id varchar(255),
        primary key (id)
    )

    create table species (
       id varchar(255) not null,
        name varchar(255),
        genus_id varchar(255),
        primary key (id)
    )

    create table subfamily (
       id varchar(255) not null,
        name varchar(255),
        family_id varchar(255),
        primary key (id)
    )

    create table subspecies (
       id varchar(255) not null,
        name varchar(255),
        species_id varchar(255),
        primary key (id)
    )

    create table subtribe (
       id varchar(255) not null,
        name varchar(255),
        tribe_id varchar(255),
        primary key (id)
    )

    create table team (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    create table tribe (
       id varchar(255) not null,
        name varchar(255),
        subfamily_id varchar(255),
        primary key (id)
    )

    create table vehicle_engine (
       id varchar(255) not null,
        manufacturer_id varchar(255),
        primary key (id)
    )

    create table vehicle_manufacturer (
       id varchar(255) not null,
        name varchar(255),
        primary key (id)
    )

    alter table if exists airport 
       add constraint fk_airport_latlon 
       foreign key (lat_lon_id) 
       references lat_lon

    alter table if exists ball 
       add constraint fk_ball_batsman 
       foreign key (batsman_id) 
       references human

    alter table if exists ball 
       add constraint fk_ball_bowler 
       foreign key (bowler_id) 
       references human

    alter table if exists ball 
       add constraint fk_ball_over 
       foreign key (over_id) 
       references over

    alter table if exists building_floor 
       add constraint fk_floor_building 
       foreign key (building_id) 
       references building

    alter table if exists car 
       add constraint FKdr4movr77cm8s34r8jtv9ajvi 
       foreign key (model_id) 
       references car_model

    alter table if exists car_model 
       add constraint FKeqxwh70fh7twe2yty3kiucpwf 
       foreign key (engine_id) 
       references vehicle_engine

    alter table if exists car_model 
       add constraint FKbhf1nita7970hg0wyeo4l4v3v 
       foreign key (manufacturer_id) 
       references company

    alter table if exists class 
       add constraint FKhd648j0bo0ujuy4ubivfx6555 
       foreign key (phylum_id) 
       references phylum

    alter table if exists country 
       add constraint fk_country_landmass 
       foreign key (landmass_id) 
       references landmass

    alter table if exists cricket_player 
       add constraint fk_cricketplayer_human 
       foreign key (human_id) 
       references human

    alter table if exists cricket_player 
       add constraint fk_team_players 
       foreign key (players_id) 
       references team

    alter table if exists currency_countries_used 
       add constraint FKr4qcylafjm27gbofcfv3lo7md 
       foreign key (countries_used_id) 
       references country

    alter table if exists currency_countries_used 
       add constraint fk_currency_countries 
       foreign key (currency_id) 
       references currency

    alter table if exists dialect 
       add constraint fk_dialect_language 
       foreign key (language_id) 
       references language

    alter table if exists dwelling 
       add constraint fk_dwelling_building 
       foreign key (building_id) 
       references building

    alter table if exists family 
       add constraint FKjvyd83xv96h3af55pnygypluu 
       foreign key (order_id) 
       references orderr

    alter table if exists genus 
       add constraint FKeyuifdoiqt6wo13n8hmqtgoq2 
       foreign key (subtribe_id) 
       references subtribe

    alter table if exists human 
       add constraint fk_human_father 
       foreign key (father_id) 
       references human

    alter table if exists human 
       add constraint fk_human_mother 
       foreign key (mother_id) 
       references human

    alter table if exists human_settlement 
       add constraint fk_humansettlement_country 
       foreign key (country_id) 
       references country

    alter table if exists identity 
       add constraint fk_identity_human 
       foreign key (human_id) 
       references human

    alter table if exists innings 
       add constraint fk_innings_match 
       foreign key (match_id) 
       references match

    alter table if exists innings 
       add constraint fk_innings_team 
       foreign key (team_id) 
       references team

    alter table if exists landmass 
       add constraint fk_landmass_planet 
       foreign key (planet_id) 
       references planet

    alter table if exists language_countries_spoken 
       add constraint FK2lqp9hudewybdoq1akpj6sft1 
       foreign key (countries_spoken_id) 
       references country

    alter table if exists language_countries_spoken 
       add constraint fk_language_countriesspoken 
       foreign key (language_id) 
       references language

    alter table if exists match 
       add constraint fk_match_ground 
       foreign key (ground_id) 
       references ground

    alter table if exists match 
       add constraint fk_match_series 
       foreign key (series_id) 
       references series

    alter table if exists orderr 
       add constraint FKghgfubyq9els64h28y6ogghti 
       foreign key (clas_id) 
       references class

    alter table if exists over 
       add constraint fk_over_session 
       foreign key (session_id) 
       references session

    alter table if exists person 
       add constraint fk_person_human 
       foreign key (human_id) 
       references human

    alter table if exists phylum 
       add constraint FKjasntrmqv0lkja5xcpul1h73o 
       foreign key (kingdom_id) 
       references kingdom

    alter table if exists planet 
       add constraint fk_planet_solarsystem 
       foreign key (solar_system_id) 
       references solar_system

    alter table if exists result 
       add constraint fk_result_ball 
       foreign key (ball_id) 
       references ball

    alter table if exists series 
       add constraint fk_series_awayteam 
       foreign key (away_team_id) 
       references team

    alter table if exists series 
       add constraint fk_series_hometeam 
       foreign key (home_team_id) 
       references team

    alter table if exists session 
       add constraint fk_session_innings 
       foreign key (innings_id) 
       references innings

    alter table if exists solar_system 
       add constraint fk_solarsystem_galaxy 
       foreign key (galaxy_id) 
       references galaxy

    alter table if exists species 
       add constraint FKcsgr3364mjx8kmkklv8sg5km 
       foreign key (genus_id) 
       references genus

    alter table if exists subfamily 
       add constraint FKbdnfo5coqk5eqlqic998k2wvq 
       foreign key (family_id) 
       references family

    alter table if exists subspecies 
       add constraint FKdue4cnw1xnbmt2aacvcw6i8vd 
       foreign key (species_id) 
       references species

    alter table if exists subtribe 
       add constraint FKm5fkrovx8y0u1fdp1slftwb0o 
       foreign key (tribe_id) 
       references tribe

    alter table if exists tribe 
       add constraint FKri14ulu7k2u6tx08jowxqpe4w 
       foreign key (subfamily_id) 
       references subfamily

    alter table if exists vehicle_engine 
       add constraint FKhhafwv124etf0qnxjoyb96vd6 
       foreign key (manufacturer_id) 
       references company
